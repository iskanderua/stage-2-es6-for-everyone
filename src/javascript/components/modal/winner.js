import { showModal } from './modal'
import { createElement } from '../../helpers/domHelper';

export function showWinnerModal(fighter) {
  // call showModal function 
  const model = {title: `Winner: ${fighter.name}`, bodyElement: createFighterImage(fighter)};
  showModal(model);
};

function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });
  return imgElement;
}
