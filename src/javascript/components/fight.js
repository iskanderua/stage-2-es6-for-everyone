import { controls } from '../../constants/controls';

const pressedKeys = new Set();
let winner;
let Fighter1, Fighter2;
let CriticalHitAvailable1 = true;
let CriticalHitAvailable2 = true;
const CriticalHitTimeOut = 10000;

export async function fight(firstFighter, secondFighter) {
  return new Promise(async (resolve) => {
    // resolve the promise with the winner when fight is over
    window.document.addEventListener('keydown', onKeyDown);
    window.document.addEventListener('keyup', onKeyUp);
    Fighter1 = firstFighter;
    Fighter2 = secondFighter;
    Fighter1.health_left = Fighter1.health;
    Fighter2.health_left = Fighter2.health;
    while (!winner) {
      await new Promise(r => setTimeout(r, 500));
    }
    window.document.removeEventListener('keydown', onKeyDown);
    window.document.removeEventListener('keyup', onKeyUp);
    resolve(winner);
  });
}
function onKeyDown(event /*KeyboardEvent*/) {
  pressedKeys.add(event.code);
  if (pressedKeys.has(controls.PlayerOneAttack)) {
    if (!pressedKeys.has(controls.PlayerTwoBlock)) {
      Fighter2.health_left -= Math.min(Fighter2.health_left, getDamage(Fighter1, Fighter2));
    }
    pressedKeys.delete(event.code);
  }
  if (pressedKeys.has(controls.PlayerTwoAttack)) {
    if (!pressedKeys.has(controls.PlayerOneBlock)) {
      Fighter1.health_left -= Math.min(Fighter1.health_left, getDamage(Fighter2, Fighter1));
    }
    pressedKeys.delete(event.code);
  }
  if (controls.PlayerOneCriticalHitCombination.every(x => pressedKeys.has(x))) {
    if (CriticalHitAvailable1) {
      CriticalHitAvailable1 = false;
      Fighter2.health_left -= Math.min(Fighter2.health_left, 2 * Fighter1.attack);
      setTimeout(() => CriticalHitAvailable1 = true, CriticalHitTimeOut);
    }
    controls.PlayerOneCriticalHitCombination.every(x => pressedKeys.delete(x));
  } 
  if (controls.PlayerTwoCriticalHitCombination.every(x => pressedKeys.has(x))) {
    if (CriticalHitAvailable2) {
      CriticalHitAvailable2 = false;
      Fighter1.health_left -= Math.min(Fighter1.health_left, 2 * Fighter2.attack);
      setTimeout(() => CriticalHitAvailable2 = true, CriticalHitTimeOut);
    }
    controls.PlayerTwoCriticalHitCombination.every(x => pressedKeys.delete(x));
  }
  updateFighterIndicators('left', Fighter1);
  updateFighterIndicators('right', Fighter2);
  if (Fighter1.health_left <= 0) {winner = Fighter2;}
  else if (Fighter2.health_left <= 0) {winner = Fighter1;}
}
function onKeyUp(event /*KeyboardEvent*/) {
  pressedKeys.delete(event.code);
}

export function getDamage(attacker, defender) {
  let damage = getHitPower(attacker) - getBlockPower(defender);
  return Math.max(0, damage);
}

function updateFighterIndicators(position, fighter) {
  const elem = document.getElementById(`${position}-fighter-indicator`);

  elem.style.width = `${fighter.health_left/fighter.health*100}%`;
}

export function getHitPower(fighter) {
  const criticalHitChance = 1 + Math.random();
  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  const dodgeChance = 1 + Math.random();
  return fighter.defense * dodgeChance;
}
